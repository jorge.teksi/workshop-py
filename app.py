from flask import Flask
import json

app = Flask(__name__)

@app.route("/")
def hello():
    return "<p>¡Hola desde Flask!</p>"

@app.route("/me")
def me_api():
    app.logger.warning('llamada a api ')
    return {"username": "jose", "theme": "black", "image": "https://picsum.photos/200/200"}
    
   