# App Web python


Aplicación web dessarrollada con python, usando el framework Flask 

## Dependencias

* Python 3 [ver](https://www.python.org/downloads/) 
* Flask [ver](https://flask.palletsprojects.com/en/2.3.x/) 


## Guía rápida


### Crear ambiente virtual de python

En una terminal de Powershell o bash (dependiendo de tu S.O.), ejecuta lo siguiente

```bash

> mkdir taller_python
> cd taller_python
> py -3 -m venv .venv

```

### Activar ambiente virtual de python

En una terminal, ejecuta lo siguiente

```bash

> .venv\Scripts\activate

```

### Instalar Flask

Dentro del entorno activado, usa el siguiente comando para instalar Flask:

```bash

$ pip install Flask

```

### Crear app inicial

Crear un archivo con el nombre __app.py__ con el siguiente contenido:

```bash

from flask import Flask

app = Flask(__name__)

@app.route("/")
def hello():
    return "<p>¡Hola desde Flask!</p>"

```


### Ejecutar una app

Para ejecutar la aplicación, hay que usar el comando __flask__, tambien podemos usar la opción __python -m flask__ 

```bash

$ flask --app app run
* Serving Flask app 'app'
* Running on http://127.0.0.1:5000
Press CTRL+C to quit

```

